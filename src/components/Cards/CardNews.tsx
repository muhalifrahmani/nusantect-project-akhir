import React from 'react'

interface ICard {
  image: string
  category: string
  title: string
  date: string
  size: string
}

function CardNews(props: ICard) {
  const { image, title, category, date, size } = props
  return (
    <div className={`${size} h-[373px] rounded-[12px] flex flex-col gap-4`}>
      <img className="rounded-[12px]" src={image} alt="" />
      <h6 className="text-[20px] text-[#404258] font-semibold">{title}</h6>
      <div className="flex gap-x-2 -mt-4">
        <p className="font-medium text-[#DF1E36] text-[14px]">{category}</p>
        <span className="w-[7px] h-[7px] bg-[#A1A5B7] rounded-full mt-[6px]" />
        <p className="text-[#A1A5B7] text-[14px]">{date}</p>
      </div>
    </div>
  )
}

export default CardNews
